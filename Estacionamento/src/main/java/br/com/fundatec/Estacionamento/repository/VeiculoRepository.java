package br.com.fundatec.Estacionamento.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.fundatec.Estacionamento.entity.Cliente;
import br.com.fundatec.Estacionamento.entity.Veiculo;

public interface VeiculoRepository extends CrudRepository<Veiculo, Integer> {
	
	
}
