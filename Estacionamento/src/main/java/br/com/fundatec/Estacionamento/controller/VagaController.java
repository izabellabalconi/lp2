package br.com.fundatec.Estacionamento.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fundatec.Estacionamento.entity.Vaga;
import br.com.fundatec.Estacionamento.entity.Veiculo;
import br.com.fundatec.Estacionamento.service.VagaService;
import br.com.fundatec.Estacionamento.service.VeiculoService;

@Controller
@RequestMapping("/api/vagas")
public class VagaController {
	@Autowired
    VagaService vagaService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Vaga> listarTodos() {
		return vagaService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Vaga buscarPorID(@PathVariable Integer id) {
		return vagaService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Vaga salvarNovo(@RequestBody Vaga vaga) {
		return vagaService.salvar(vaga);
	}	
}
