package br.com.fundatec.Estacionamento.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiResponse;

import br.com.fundatec.Estacionamento.entity.Cliente;
import br.com.fundatec.Estacionamento.service.ClienteService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/clientes")
public class ClienteController {
	
	@Autowired
    ClienteService clienteService;

	@GetMapping(value = "/")
	@ApiOperation(value = "Listar todos os clientes",
    notes = "Lista todos os clientes do sistema.")
	@ApiResponses(value = {
    @ApiResponse(code = 200, message = "Clientes listados com sucesso", response = Cliente.class),
	})
	
	public ResponseEntity<List<Cliente>> listarTodos() {
		return ResponseEntity.ok(clienteService.buscarTodos());
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Cliente buscarPorID(@PathVariable Integer id) {
		return clienteService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Cliente salvarNovo(@RequestBody Cliente cliente) {
		return clienteService.salvar(cliente);
	}	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Cliente editar(@PathVariable Integer id, @RequestBody Cliente cliente) {
		return clienteService.editar(id, cliente);
	}
}
