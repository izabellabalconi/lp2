package br.com.fundatec.Estacionamento.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.fundatec.Estacionamento.entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	Cliente findByNome(String nome);
	Cliente findByCpf(String cpf);
}
