package br.com.fundatec.Estacionamento.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.fundatec.Estacionamento.entity.Vaga;


public interface VagaRepository extends CrudRepository<Vaga, Integer>{
	
	

}
