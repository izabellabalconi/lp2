package br.com.fundatec.Estacionamento.entity;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "VAGA")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class Vaga {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "VAGA_SEQ", sequenceName = "VAGA_SEQ")
	@GeneratedValue(generator = "VAGA_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	private boolean vagaLivre;
	private LocalDateTime horaEntrada;
	private LocalDateTime horaSaida;
	@ManyToOne
	@JoinColumn(name = "id_cliente")
	private Cliente cliente;	

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Integer getId() {
		return id;
	}

	
	public double calcularHoras() {
	
	return horaSaida.getHour() - horaEntrada.getHour();
	
	}

	public double definirValorAPagar() {
		if (horaEntrada.getHour() >= 9 && horaEntrada.getHour() <= 18 && this.calcularHoras() < 24) {
			return (this.calcularHoras() *2) + 5;
		}
		else if(this.calcularHoras() > 24) {
			return 20;
		}
		
		return (this.calcularHoras() *4) + 8;

	}	
	
		
	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isVagaLivre() {
		return vagaLivre;
	}

	public void setVagaLivre(boolean vagaLivre) {
		this.vagaLivre = vagaLivre;
	}

	public LocalDateTime getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(LocalDateTime horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public LocalDateTime getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(LocalDateTime horaSaida) {
		this.horaSaida = horaSaida;
	}
}
