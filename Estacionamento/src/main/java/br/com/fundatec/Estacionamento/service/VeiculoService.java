package br.com.fundatec.Estacionamento.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fundatec.Estacionamento.entity.Veiculo;
import br.com.fundatec.Estacionamento.repository.VeiculoRepository;

@Service
public class VeiculoService {
	@Autowired
	public VeiculoRepository veiculoRepository;
	

	public Veiculo salvar(Veiculo veiculo) {
		return veiculoRepository.save(veiculo);
	}
	
	public Veiculo buscarPorID(Integer id) {
		if( veiculoRepository.findById(id).isPresent() )
			return veiculoRepository.findById(id).get();
		return null;
	}
	
	public List<Veiculo> buscarTodos() {
		return (List<Veiculo>) veiculoRepository.findAll();
	}

}
