package br.com.fundatec.Estacionamento.service;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fundatec.Estacionamento.entity.Cliente;
import br.com.fundatec.Estacionamento.entity.Vaga;
import br.com.fundatec.Estacionamento.repository.VagaRepository;

@Service
public class VagaService {
	@Autowired
	public VagaRepository vagaRepository;

	public Vaga salvar(Vaga vaga) {
		return vagaRepository.save(vaga);
	}

	public Vaga buscarPorID(Integer id) {
		if (vagaRepository.findById(id).isPresent())
			return vagaRepository.findById(id).get();
		return null;
	}

	public List<Vaga> buscarTodos() {
		return (List<Vaga>) vagaRepository.findAll();
	}
	public void ocuparVaga(Vaga vaga, Cliente cliente) {
		vaga.setVagaLivre(false);
		vaga.setCliente(cliente);
		vaga.setHoraEntrada(LocalDateTime.now());
		vagaRepository.save(vaga);
		
	}
	public void desocuparVaga(Vaga vaga, Cliente cliente) {
		vaga.setVagaLivre(true);
		vaga.setHoraSaida(LocalDateTime.now());
		//vaga.definirValorAPagar()
		//TODO: refatorar estes m�todos para conseguir calcular o valor 
		
	}
	
}
