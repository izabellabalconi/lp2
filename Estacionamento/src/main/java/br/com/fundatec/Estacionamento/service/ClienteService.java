package br.com.fundatec.Estacionamento.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fundatec.Estacionamento.entity.Cliente;
import br.com.fundatec.Estacionamento.repository.ClienteRepository;

@Service
public class ClienteService {
	@Autowired
	public ClienteRepository clienteRepository;
	
	
	public Cliente salvar(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	public Cliente buscarPorID(Integer id) {
		if( clienteRepository.findById(id).isPresent() )
			return clienteRepository.findById(id).get();
		return null;
	}
	
	public List<Cliente> buscarTodos() {
		return (List<Cliente>) clienteRepository.findAll();
	}

	public Cliente editar(Integer id, Cliente cliente) {
		cliente.setId(id);
		return clienteRepository.save(cliente);
	}
	public void deletar(Cliente cliente) {
		clienteRepository.delete(cliente);
	}
}
