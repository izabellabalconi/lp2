package br.com.fundatec.Estacionamento.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fundatec.Estacionamento.entity.Cliente;
import br.com.fundatec.Estacionamento.entity.Veiculo;
import br.com.fundatec.Estacionamento.service.ClienteService;
import br.com.fundatec.Estacionamento.service.VeiculoService;

@Controller
@RequestMapping("/api/veiculos")
public class VeiculoController {
	
	@Autowired
    VeiculoService veiculoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Veiculo> listarTodos() {
		return veiculoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Veiculo buscarPorID(@PathVariable Integer id) {
		return veiculoService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Veiculo salvarNovo(@RequestBody Veiculo veiculo) {
		return veiculoService.salvar(veiculo);
	}	


}
