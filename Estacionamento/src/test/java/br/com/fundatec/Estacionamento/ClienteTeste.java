package br.com.fundatec.Estacionamento;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;

import br.com.fundatec.Estacionamento.entity.Cliente;
import br.com.fundatec.Estacionamento.entity.Veiculo;
import br.com.fundatec.Estacionamento.repository.ClienteRepository;

public class ClienteTeste extends EstacionamentoApplicationTests {	
	
	@MockBean
	private ClienteRepository clienteRepository;
	
			
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		Cliente cliente = new Cliente();		
		cliente.setNome("Tiago");
		cliente.setCpf("123456789");
		
		Veiculo chevetao = new Veiculo();
		Veiculo fuca = new Veiculo();
		
		List<Veiculo> veiculos = new ArrayList<>();
		cliente.setVeiculos(veiculos);
		
		veiculos.add(chevetao);
		veiculos.add(fuca);
		
		
		Mockito.when(clienteRepository.findByNome(cliente.getNome())).thenReturn(cliente);
		Mockito.when(clienteRepository.findByCpf(cliente.getCpf())).thenReturn(cliente);		
		
	}
	
	@Test
	public void VerificarNomeCliente() {
		String nome = "Tiago";
		
		Cliente found = clienteRepository.findByNome(nome);
		
		assertThat(found.getNome()).isEqualTo(nome);
	}
	
	
	@Test
	public void VerificarCPFCliente() {
		String cpf = "123456789";
		
		Cliente found = clienteRepository.findByCpf(cpf);
		
		assertThat(found.getCpf()).isEqualTo(cpf);
	}
	
	//TODO: teste pra pegar a lista de veículos
}
